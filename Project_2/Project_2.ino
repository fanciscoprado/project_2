#include <EEPROM.h>
#include <SimpleDHT.h>
#include <LiquidCrystal.h> 
#include <DS3231_Simple.h>
#define left 1
#define right 2
#define up 3
#define down 4 
#define enter 5
#include <FastLED.h>
#include <SPI.h>          // needed for Arduino versions later than 0018
#include <Ethernet.h>
#include <EthernetUdp.h>  // UDP library from: bjoern@cs.stanford.edu 12/30/2008
#define t_LED 2
#define t_D13 3
#define t_ON 4
#define t_OFF 5
#define t_GREEN 6
#define t_SET 9
#define t_STATUS 10
#define t_LEDS 11
#define t_VERSION 12
#define t_HELP 113
#define t_RED 14
#define t_BLINK 7
#define t_OSC 15
#define t_Num 20
#define t_BREAK 21
#define t_ADD 22
#define t_DATE 23
#define t_TIME 24
#define t_RGB 25
#define t_TEMP 26
#define t_HIS 27
#define t_HIGH 28
#define t_BRIGHT 29
#define t_NET 30
CRGB leds[4];
bool printIT = false;
DateTime MyDateAndTime;
SimpleDHT22 dht22(9);
byte tempCursor = 9;
 int Contrast=75;
 //LiquidCrystal lcd(12, 11, 5, 4, 3, 2); 
 LiquidCrystal lcd(A0, A1, A2, 5, 7, 2); 
 EthernetUDP Udp;
 bool isUdpPacket = false;
 byte highTemp, lowTemp;
 unsigned long previousMillisTemp =0;
 unsigned long previousSent=0;
 unsigned long previousRecived=0;
 int updateInterval = 2000;
 byte alarm=0;
 float temp, humid;
 char packetBuffer[UDP_TX_PACKET_MAX_SIZE];  //buffer to hold incoming packet,
 char sentPacket[UDP_TX_PACKET_MAX_SIZE];
 char recivedPacket[UDP_TX_PACKET_MAX_SIZE];
 char previousPacketSent[UDP_TX_PACKET_MAX_SIZE];
 char lookup[22][5]; // array with recognizable tokens
char  ReplyBuffer[] = "acknowledged";       // a string to send back
 long tempCheck = 5000;
 unsigned long previousMillisTH = 0;
 byte theCursor = 0;
 byte cursorPosition;
 bool updoot; 
 byte currentScreenV = 0;
 byte currentScreenH = 0;
 byte lastPrinted = -1;
 byte lastPrintedd = -1;
 byte isRgbOn = 0;
 byte saveTo;
 byte preColor[] = {255, 0, 0};
 char command[20];
 byte net0, net1, net2, net3= 0;
 byte vMax =0;
 byte cmdIndex = 0;
 unsigned int thStatusInt = 5000;
 int lastAdress= -1;
 int location =0; 
 byte preTH = 1;
 byte coolDownInterval = 0;
 unsigned long previousMillisCoolDown = 0;
 char* screen0[] = {""};
 char* screen1[] = {">Temp History", ">Packet Status", ">Settings"};
 char* screen2[] ={"uhhh history scroll"};
 char* screen3[] ={">Sent Packet",">Recived Packet"};
 char* screen4[] ={">Change IP",">Change subnet", ">Change Gateway", ">Erase History", ">Adjust Alarm"};
 char* screen7[] ={">Major Under",">Minor Under", ">Comfurtable", ">Minor over", ">Major Over"};
 char** screens[] = {screen0, screen1, screen2, screen3, screen4, screen2, screen2, screen7};
 IPAddress ipRemote(192, 168,0,77);
unsigned int portRemote = 36801;  
DS3231_Simple Clock;

 // ip screen 5------------ sub screen 6------------- gate screen 7
unsigned int localPort = 8888;  
long add(unsigned int a, unsigned int b){
  return (long)(a+b);
}

void addToList(byte spot, byte wLength, char string[], byte last){
  byte i = 0;
  for(i = 0; i < 2; i++){
    lookup[spot][i]= string[i];
  }
  lookup[spot][i] = wLength;
  i++;

  lookup[spot][i] = last;

}



 void loadNet(){
  byte mac[] = {  
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
  saveTo = 0;
  readFrom();
  IPAddress ip(net0, net1,net2,net3);
  unsigned int localPort = 8888;  
  saveTo =2;
  readFrom();
  byte gateway[] = {
  net0, net1, net2, net3 };  // gateway address
  saveTo =1;
  readFrom();
byte subnet[] = {
  net0, net1, net2, net3};  //subnet mask


  Ethernet.begin(mac, ip, gateway, subnet);
 }
 void setDate(unsigned int month, unsigned int day, unsigned int year){
  if(month > 12 || day > 32 || year >99){
    Serial.println(F("Invalid entry"));
  }else{
    Serial.println(year);
    MyDateAndTime.Day    = day;
    MyDateAndTime.Month  = month;
    MyDateAndTime.Year   = (int)year;
    // Then write it to the clock
    Clock.write(MyDateAndTime);
  }
  // And use it, we will read it back for example... 
  if(isUdpPacket){
  Udp.beginPacket(ipRemote,portRemote);
  Udp.print("Time has been set");
  Udp.endPacket(); 
  }else{
  Serial.print(F("The time has been set to: "));
  Clock.printTo(Serial);
  }
  
  
}
 void printTime(){
  
if(isUdpPacket){
  Udp.beginPacket(ipRemote,portRemote);
  Clock.printTo(Udp);
  Udp.endPacket();
  
}
Clock.printTo(Serial);
  
}


 void rgbOn(){
//  isRgbOn = 1;
  leds[0].setRGB(preColor[0], preColor[1], preColor[2]);
  
  FastLED.show(); 
}

unsigned int byte2int(byte a, byte b){
 // unsigned int number = a <<8 | b ;
  return a <<8 | b ;;
}

void tokenizeNumber(char wordToken[], byte wordLength){
  unsigned int temp = char2int(wordToken, wordLength);
  unsigned char bytes[2];
  bytes[0] = (temp >> 8) & 0xFF;
  bytes[1] = temp & 0xFF;

  command[cmdIndex] = bytes[0];
  cmdIndex++;
  command[cmdIndex] = bytes[1];
  cmdIndex++;
}
unsigned int char2int (char arrayy[], size_t n)
{    
    long number = 0;
    long mult = 1;
    /* for each character in array */
    while (n--)
    {
      /* convert digit to numeric value   */
      number += (arrayy[n] - '0') * mult;
      mult *= 10;
        
    }
    if(number > 65535){
      command[cmdIndex] = t_BREAK;
      cmdIndex++;
      return 1 ;
    }
    command[cmdIndex] = t_Num;
    cmdIndex++;
    return (unsigned int)number;
}
void packetChecker(int packetSize, byte x){
if(x == 0){
  for(int i=0; i < packetSize; i ++){
    recivedPacket[i] = packetBuffer[i];
  }
  recivedPacket[packetSize] = '\0';
}else{

}
  
}
void setRGB(int r, int g,int b){
  if(r > 255 || g > 255 || b > 255){
    Serial.println("Error Wrong Values");
  }else{
    preColor[0] =(byte) r;
    preColor[1] =(byte) g;
    preColor[2] =(byte) b;
    leds[0].setRGB( r, g, b);
    if(isRgbOn == 1)
      FastLED.show();
  }
}
void setTime(unsigned int hour, unsigned int minute){
  if(hour > 23 || minute >59){
    Serial.println(F("Invalid entry"));
  }else{
    MyDateAndTime.Hour   = hour;
    MyDateAndTime.Minute = minute;
    // Then write it to the clock
    Clock.write(MyDateAndTime);
  }
}
void searchComand(){
  switch(command[0]){
    case t_TEMP:
      switch (command[1]){
        case t_HIS:
          //printEEPROM();
          break;
        case t_STATUS:
          //printTemp();
          break;
        case t_HIGH:
          //printHighLow();
          break;
      }
    case t_RGB:
      switch(command[1]){
        case t_BLINK:
          //rgbBlink = 1;
          break;
        case t_ON:
          rgbOn();
          //rgbBlink = 0;
          break;
        case t_OFF:
          rgbOff();
          //rgbBlink = 0;
          break;
        case t_SET:
          if(command[2] == t_BRIGHT){
            if(command[3] < 255)
            //command to change brightness
            break;
          }
          if(command[2] == command[5] && command[5] == command[8] && command[8] == t_Num){
            setRGB(byte2int(command[3],command[4]), byte2int(command[6],command[7]), byte2int(command[9],command[10]));
          }
          break;
      }
      break;
    case t_DATE:
      printTime();
      
      break;
    case t_LED:
      switch(command[1]){
        case t_BLINK:
          //blinkBigLedIt=1;
          //isOscillate = 0;
          //pattern = 0x44;
          switch (command[2]){
            case t_RED:
             // pattern = 0x44;
              break;
            case t_GREEN:
              //pattern = 0x88;
              break;
            case t_OSC:
              //pattern = 0x66;
              break;
          }
          break;
        case t_GREEN:
          //set led green
          //color = t_GREEN;
          //if(offBoardLed == 1)
            //bgLedOn();
            //pattern = 0xaa;
            
          break;
        case t_RED:
          //pattern = 0x55;
          //set led red
         // color = t_RED;
         // if(offBoardLed == 1)
          //  bgLedOn();
          break;
        case t_OFF:
          //bgLedOff();
          //blinkBigLedIt = 0;
          //prePattern = pattern;
          //pattern = 0x00;
          //offBoardLed = 0;
          //isOscillate = 0;
          break;
        case t_ON:
         // bgLedOn();
         // blinkBigLedIt = 0;
         //pattern = prePattern;
          //offBoardLed = 1;
          break;
        case t_OSC:
          //isOscillate = 1;
          //blinkBigLedIt=0;
          break;
      }
      break;

      case t_SET:
      switch(command[1]){
        case t_TIME:
          if(command[2] == command[5] && command[5] == t_Num){
            setTime(byte2int(command[3],command[4]), byte2int(command[6],command[7]));
          }else{ /*Serial.println("Invalid entry");*/}
          break;          

        case t_DATE:
          if(command[2] == command[5] && command[5] == command[8] && command[8] == t_Num){
            setDate(byte2int(command[3],command[4]), byte2int(command[6],command[7]), byte2int(command[9],command[10]));
          }
          
      }
      break;

    case t_VERSION:
      //print version
      if(isUdpPacket){
            Udp.beginPacket(ipRemote,portRemote);
            Udp.write("Version 3.1");

            Udp.endPacket();

      }else{
        //Serial.print("Version 3.1");
      }
 
      break;
    case t_HELP:
      //print help
      //printHelp();
      
      break;
    case t_ADD:
      switch (command[1]){
                case t_BREAK:
                  Serial.print("out of bounds");
                  //newLine();
                  break;
                case t_Num:
               
                    switch (command[4]){
                      case t_BREAK:
                        Serial.print("out of bounds");
                        //newLine();
                        break;
                      case t_Num:
                        if(isUdpPacket){
                            Udp.beginPacket(ipRemote,portRemote);
                            Udp.print(add(byte2int(command[2],command[3]), byte2int(command[5],command[6]) ));
                            Udp.endPacket();
                        }
                       // newLine();
                        Serial.print(add(byte2int(command[2],command[3]), byte2int(command[5],command[6]) ));
                        break;
                  }
                  break;
              }
      break;
      
  }
  
}

 
 void makeToken(byte wordLength, char toToken[]){
  if(isNumber(wordLength, toToken) == 1){
    tokenizeNumber(toToken, wordLength);
    
  }else{
    for(byte i=0; i < 22; i++){//INCREAS THIS NUMBER WHEN WORD IS ADDED < 'NEW NUMBER'
  
      if(toToken[0] == lookup[i][0]){
  
        if(toToken[1] == lookup[i][1]){
          if(wordLength == lookup[i][2]){
            command[cmdIndex] = lookup[i][3];
            cmdIndex++;
            break;
          }
        }
  
      }
    }
  }
  

}
 int fahrenheit(float x){
  return(int) (x*9.0)/ 5.0 +32;
}


void parseString(char inData[]){
  cmdIndex = 0;
  for(byte k =0; k<3;k++){
    command[k] = 255;
  }
  int wordLength = 0;
  byte i = 0;
  byte j = 0;
  char toToken[20];
  
  while(inData[i] != 13){
   
    
    if(inData[i] == 32){
    //code in here
    toToken[j]= '\0';
      //newLine();
        byte l=0;
  makeToken(wordLength, toToken);

      wordLength = 0;
      j = -1;
    }else{
      toToken[j] = inData[i];
      //Serial.print(inData[i]);
      wordLength++;
    }
    i++;
    j++;
  }
  toToken[j]= '\0';
  //newLine();
  makeToken(wordLength, toToken);
 
  searchComand();

}
byte isNumber(byte wordLength, char toToken[]){
  byte isToken =0;
  for(int i = 0; i < wordLength; i++){
    if(toToken[i] >= 48 && toToken[i] <= 57){
      isToken = 1;
      
    }else{
      return 0;
      
    }
  }
  
  return isToken;
}


void sendPacket(byte threshold ){
    float temperature = 0;
  float humidity = 0;
  int err = SimpleDHTErrSuccess;
  if ((err = dht22.read2(&temperature, &humidity, NULL)) != SimpleDHTErrSuccess) {
    
 
  }
  temperature = fahrenheit(temperature);
    Udp.beginPacket(ipRemote,portRemote);
    switch (threshold){
      case 1:
        Udp.write("Alert temp is now major under. Curent time: ");
        
        break;
      case 2:
        Udp.write("Alert temp is now Minow Under. Curent time: ");
        break;
      case 3:
        Udp.write("Alert temp is currently Comfortable. Curent time: ");
        break;
      case 4:
        Udp.write("Alert temp is now Minor Over. Curent time: ");
        break;
      case 5:
        Udp.write("Alert temp is now Major Over. Curent time: ");
        break;
      
    }
    //Clock.printTo(Udp);
    Udp.write("---Curent temp:   ");
    Udp.print(fahrenheit(temp));
    Udp.write(" F*.");

    Udp.endPacket();
}
 void showSent(){
 // if(lastPrinted != 10 || strcmp(previousPacketSent,  sentPacket) !=0){
  lcd.clear();
  lcd.print(sentPacket);
  lastPrinted = 10;
 
 // }
 }

 void printRecived(){
  if(millis() - previousRecived > updateInterval){
  Serial.println('*');
  Serial.println(recivedPacket);
  lcd.clear();
  lcd.print(recivedPacket);
  previousRecived = millis();
  }
  lastPrinted=11;
 }

 
 void homeScreen(){
 // if(lastPrinted == 0){}else{
 if(millis()- previousMillisTemp > tempCheck-2 || lastPrinted != 0){
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Temp:");
  //print tmep here
  lcd.print(temp);
  lcd.setCursor(0,1);
  lcd.print("Humidity:");
  lcd.print(humid);
  lcd.print("%");
  //print humididty here
  lastPrinted = 0;
  }

  

  //add if staement to check if need to change temp humid numbers numbers
 }

void setStatusRGB(byte state){
  switch (state){
    case 1:
      leds[1] = CRGB::Purple;
      break;
    case 2:
      leds[1] = CRGB::Blue;
      break;
    case 3:
      leds[1] = CRGB::Green;
      break;
    case 4:
      leds[1] = CRGB::Orange;
      break;
    case 5:
      leds[1] = CRGB::Red;
      break;
     
  }
  FastLED.show();
}

void checkState(){
  float temperature = 0;
  float humidity = 0;
  int err = SimpleDHTErrSuccess;

  temperature = fahrenheit(temp);
  if((int)temperature <= 60){
    if(preTH != 1){
      sendPacket(1);
      preTH = 1;
      setStatusRGB(1);
    }
  }
  if((int)temperature > 60 && (int)temperature <= 70){
    if(preTH != 2){
      sendPacket(2);
      preTH = 2;
      setStatusRGB(2);
    }
    
  }
  if((int)temperature > 70 && (int)temperature <= 80){
    if(preTH != 3){
      sendPacket(3);
      preTH = 3;
      setStatusRGB(3);
    }
    
  }
  if((int)temperature > 80 && (int)temperature <= 90){
    if(preTH != 4){
      sendPacket(4);
      preTH = 4;
      setStatusRGB(4);
    }
    
  }
  if((int)temperature > 90){
    if(preTH != 5){
      sendPacket(5);
      preTH = 5;
      setStatusRGB(5);
    }
    
  }
  
}
void checkConection(){
  Udp.beginPacket(ipRemote,portRemote);
  Udp.write("h3llo?");
  Udp.endPacket();
  delay(5);

    int packetSize = Udp.parsePacket();
    
  if(packetSize)//This is where we process the packets
  {
    leds[2] = CRGB::Green;
    FastLED.show();
    
  }
  else{
    leds[2] = CRGB::Red;
    FastLED.show();
  }
}
 
 void printErom(){
  if(lastAdress == location){}else{
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print(EEPROM.read(location));
  lcd.print("F/RH:");
  lcd.print(EEPROM.read(location+1));
  lcd.print("%");
  lcd.setCursor(0,1);
  lcd.print(EEPROM.read(location+4));lcd.print("/");lcd.print(EEPROM.read(location+5));lcd.print("/");lcd.print(EEPROM.read(location+6));
  lcd.print(" ");
  lcd.print(EEPROM.read(location+2));lcd.print(":");if(EEPROM.read(location+3)<10){lcd.print(0);}lcd.print(EEPROM.read(location+3));
  lastAdress = location;
  }
 }
 void printNet(){
  //
  // decide what to load before called
  //save once enter is hit
  if(lastPrinted !=5 || updoot){
  lcd.clear();
  if(net0 < 100) lcd.print("0"); if(net0 < 10) lcd.print("0"); lcd.print(net0);lcd.print(".");
  if(net1 < 100) lcd.print("0"); if(net1 < 10) lcd.print("0");lcd.print(net1);lcd.print(".");
  if(net2 < 100) lcd.print("0"); if(net2 < 10) lcd.print("0");lcd.print(net2);lcd.print(".");
  if(net3 < 100) lcd.print("0"); if(net3 < 10) lcd.print("0");lcd.print(net3);
  lcd.setCursor(cursorPosition,0);
  lastPrinted = 5;
  updoot = false;
  }
 }
 void incriment(){
  switch (cursorPosition){
    case 0:
      if(net0+100 > 255){}else{
      net0 += 100;
      }
      break;
    case 4:
      if(net1+100 > 255){}else{
      net1 += 100;
      }
      break;
    case 8:
      if(net2+100 > 255){}else{
      net2 += 100;
      }
      break;
    case 12:
      if(net3+100 > 255){}else{
      net3 += 100;
      }
      break;
    case 1:
      if(net0+10 > 255){}else{
      net0 += 10;
      }break;
    case 5:
      if(net1+10 > 255){}else{
      net1 += 10;
      }
      break;
    case 9:
      if(net2+10 > 255){}else{
      net2 += 10;
      }
      break;
    case 13:
      if(net3+10 > 255){}else{
      net3 += 10;
      }
      break;
    case 2:
      if(net0+1 > 255){}else{
      net0 += 1;
      }
      break;
    case 6:
    if(net1+1 > 255){}else{
      net1 += 1;
      }
      break;
    case 10:
      if(net2+1 > 255){}else{
      net2 += 1;
      }
      break;
    case 14:
      if(net3+1 > 255){}else{
      net3 += 1;
      }
      break;
      
      //go up by huned
      
  }
  updoot = true;
 }
  void reduce(){
  switch (cursorPosition){
    case 0:
      if(net0-100 < 0){}else{
      net0 -= 100;
      }
      break;
    case 4:
      if(net1-100 < 0){}else{
      net1 -= 100;
      }
      break;
    case 8:
      if(net2-100 < 0){}else{
      net2 -= 100;
      }
      break;
    case 12:
      if(net3-100 < 0){}else{
      net3 -= 100;
      }
      break;
    case 1:
      if(net0-10 < 0){}else{
      net0 -= 10;
      }break;
    case 5:
      if(net1-10 < 0){}else{
      net1 -= 10;
      }
      break;
    case 9:
      if(net2-10 < 0){}else{
      net2 -= 10;
      }
      break;
    case 13:
      if(net3-10 < 0){}else{
      net3 -= 10;
      }
      break;
    case 2:
      if(net0-1 < 0){}else{
      net0 -= 1;
      }
      break;
    case 6:
    if(net1-1 < 0){}else{
      net1 -= 1;
      }
      break;
    case 10:
      if(net2-1 < 0){}else{
      net2 -= 1;
      }
      break;
    case 14:
      if(net3-1 < 0){}else{
      net3 -= 1;
      }
      break;
      
      //go up by huned
      
  }
  updoot = true;
 }
 void printTempChange(){
  lcd.noCursor();
  if(lastPrinted !=20 || printIT){
  lcd.clear();
  if(theCursor == 0)lcd.print(">"); lcd.print("low  ");
  if(lowTemp < 100) lcd.print("0");if(lowTemp<10)lcd.print("0");lcd.print(lowTemp);
  lcd.setCursor(0,1);
  if(theCursor == 1)lcd.print(">");lcd.print("high ");
  if(highTemp < 100) lcd.print("0");if(highTemp<10)lcd.print("0");lcd.print(highTemp);
  lastPrinted = 20;
  printIT = false;
 }
 }
 void readTemp(){
  switch(alarm){
    case 0:
      lowTemp=EEPROM.read(900);
      break;     
    case 1:
      lowTemp=EEPROM.read(901);
      highTemp=EEPROM.read(902);
      break;
    case 2:
      lowTemp=EEPROM.read(903);
      highTemp=EEPROM.read(904);
      break;
    case 3:
      lowTemp=EEPROM.read(905);
      highTemp=EEPROM.read(906);
      break;
    case 4:
      lowTemp=EEPROM.read(907);
      break;
  }
 }
 void saveTemp(){
  switch(alarm){
    case 0:
      EEPROM.write(900, lowTemp);
      break;     
    case 1:
      EEPROM.write(901, lowTemp);
      EEPROM.write(902, highTemp);
      break;
    case 2:
      EEPROM.write(903,lowTemp);
      EEPROM.write(904,highTemp);
      break;
    case 3:
      EEPROM.write(905, lowTemp);
      EEPROM.write(906,highTemp);
      break;
    case 4:
      EEPROM.write(907,lowTemp);
      break;
  }
 }
 void readFrom(){
  switch(saveTo){
    case 0:
      net0 =EEPROM.read(1014);
      net1 =EEPROM.read(1015);
      net2 =EEPROM.read(1016);
      net3 =EEPROM.read(1013);
      break;
    case 1:
      net0 =EEPROM.read(1017);
      net1 =EEPROM.read(1018);
      net2 =EEPROM.read(1019);
      net3 =EEPROM.read(1012);
      break;
    case 2:
      net0 =EEPROM.read(1020);
      net1 =EEPROM.read(1021);
      net2 =EEPROM.read(1022);
      net3 =EEPROM.read(1011);
      break;

  }
 }
void setTemp(){
     float temperature = 0;
  float humidity = 0;
  int err = SimpleDHTErrSuccess;
  if ((err = dht22.read2(&temperature, &humidity, NULL)) != SimpleDHTErrSuccess) {
    Serial.print(F("Read DHT22 failed, err=")); Serial.println(err);
    return;
  }
  temp = (float)temperature;
  humid = (float)humidity;
}
 
 void saveToo(){
  byte a, b, c, d = 0;
  a = net0; b = net1; c = net2; d = net3;
  switch(saveTo){
    case 0:
      EEPROM.write(1014, a);
      EEPROM.write(1015, b);
      EEPROM.write(1016, c);
      EEPROM.write(1013, d);
      break;
    case 1:
      EEPROM.write(1017, a);
      EEPROM.write(1018, b);
      EEPROM.write(1019, c);
      EEPROM.write(1012, d);
      break;
    case 2:
      EEPROM.write(1020, a);
      EEPROM.write(1021, b);
      EEPROM.write(1022, c);
      EEPROM.write(1011, d);
      break;
      
  }
 }
 
 void nextScreen(){
  switch(currentScreenH){
    case 1:
      switch(currentScreenV){
        case 0:
          currentScreenH = 2; currentScreenV=0; vMax=0; lastPrinted = 2;
          break;
        case 1:
          currentScreenH = 3; currentScreenV=0; vMax=2;
          break;
        case 2:
          currentScreenH = 4; currentScreenV=0; vMax=5;
          break;
      }
      break;
    case 4:
      switch(currentScreenV){
        case 0:
          currentScreenH = 5;
          saveTo = 0;
          readFrom();
          //load ip stuff
          break;
        case 1:
          //lead subnet stuff
          currentScreenH = 5;
          saveTo = 1;
          readFrom();
          break;
        case 2:
          currentScreenH = 5;
          saveTo = 2;
          readFrom();
          break;
          //load gateway stuff
        case 3:
          erase();
        case 4:
          currentScreenH=7;
          vMax = 5;
          break;
        
      }
      break;
    case 3:
      switch(currentScreenV){
        case 0:
          currentScreenH = 10; currentScreenV=0; vMax=0; lastPrinted = 2;
          break;
        case 1:
          currentScreenH = 11; currentScreenV=0; vMax=0;
          break;
        
      }
      break;
    case 7:
      switch(currentScreenV){
        case 0:
          currentScreenH = 20; currentScreenV=0; vMax=0; lastPrinted = 2;
          alarm = 0;
          readTemp();
          printTempChange();
          
          
          lcd.cursor();
          break;
        case 1:
          currentScreenH = 21; currentScreenV=0; vMax=0;
          alarm = 1;
          readTemp();
          printTempChange();
          
          lcd.cursor();
          break;
        case 2:
          currentScreenH = 22; currentScreenV=0; vMax=0;
          alarm = 2;
          readTemp();
          printTempChange();
          alarm = 2;
          lcd.cursor();
          break;
        case 3:
          currentScreenH = 23; currentScreenV=0; vMax=0;
          alarm = 3;
          readTemp();
          printTempChange();
          
          lcd.cursor();
          break;
        case 4:
          currentScreenH = 24; currentScreenV=0; vMax=0;
          alarm = 4;
          readTemp();
          printTempChange();
          
          lcd.cursor();
          break;
        
        
      }
  }

 }

 void printMenu(){
  if(lastPrinted == currentScreenH && lastPrintedd == currentScreenV){}else{
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print(screens[currentScreenH][currentScreenV]);
    lastPrinted = currentScreenH;
    lastPrintedd = currentScreenV;
  }
 }
 void rgbOff(){
  //isRgbOn = 0;
  leds[0] = CRGB::Black;
  FastLED.show();
}
 void printScreen(){
  switch(currentScreenH){
    case 0:
      homeScreen();
      break;
    case 2:
      printErom();
      break;
    case 10://-------------------------------------------000000000000000000000000000000000000000000000000000000000000000000000000000000000000
      showSent();
      break;
    case 5:
      printNet();
      break;
    case 11:
      printRecived();
      break;
    case 20:
    case 21:
    case 22:
    case 23:
    case 24:
      printTempChange();
      break;
      
      
    default:
      printMenu();
      break;
  }
  
 }

 void setup()
 {
    analogWrite(6,Contrast);
     lcd.begin(16, 2);
     FastLED.addLeds<NEOPIXEL, 8>(leds, 4);
     FastLED.setBrightness(5);
     rgbOff();
     loadNet();
     Udp.begin(localPort);
     leds[3] = CRGB::Green;

      Clock.begin();
  addToList(0, 3, "LED", t_LED);
    
  addToList(1, 5, "BLINK", t_BLINK);
  
  addToList(2, 2, "ON", t_ON );
  
  addToList(3, 3, "OFF", t_OFF);
 
  addToList(4, 5, "GREEN", t_GREEN);

  addToList(5, 3, "D13", t_D13);
 
  addToList(6, 3, "RED", t_RED);

  addToList(7, 6, "STATUS", t_STATUS);
 
  addToList(8, 4, "LEDS", t_LEDS);

  addToList(9, 7, "VERSION", t_VERSION);

  addToList(10, 4, "HELP", t_HELP);
  
  addToList(11, 3, "SET", t_SET);

  addToList(12, 9,"OSCILLATE", t_OSC);

  addToList(13, 3, "ADD", t_ADD);

  addToList(14, 4, "DATE", t_DATE);

  addToList(15, 4, "TIME", t_TIME);

  addToList(16, 3, "RGB", t_RGB);

  addToList(17, 4, "TEMP", t_TEMP);
  addToList(18, 7, "HISTORY", t_HIS);
  addToList(19, 4, "HIGH", t_HIGH);
  addToList(20, 10, "BRIGHTNESS", t_BRIGHT);
  addToList(21, 3, "NET", t_NET);


     
     Serial.begin(9600);
     setTemp();

  } 

  void navMenue(byte buttonPressed){
    switch(currentScreenH){
      case 0:
        switch(buttonPressed){
          
          case right:
            currentScreenH++;
            vMax = 3;
            break;
          
        }
        break;//end case 1
      case 1:
        switch(buttonPressed){
          case left:
            currentScreenH--;
            currentScreenV = 0;
            vMax=0;
            break;
          case down:
            if(currentScreenV+1 < vMax)
              currentScreenV++;
            else{currentScreenV = 0;};
            break;
          case up:
            if(currentScreenV <= 0){currentScreenV = vMax -1;}else{
            currentScreenV--;
            }
            break;
          case right:
            nextScreen();
            break;
          
        }
        break;//end case 1
      case 2:
        switch(buttonPressed){
          case left:
            currentScreenH = 1; currentScreenV = 0; vMax=3;
            break;
          case down:
           if(EEPROM.read(location+7) == 0){}else{
            location+=7;
            }
            break;
          case up:
            if(location == 0){}else{
              location-=7;
            }
        }
        break;//ende case 2
      case 3://--------------------------------------------------------------------------------------------------------------------------------------<
        switch(buttonPressed){
          case left:
            currentScreenH = 1; currentScreenV = 0; vMax=3;
            break;
          case down:
            if(currentScreenV+1 < vMax)
              currentScreenV++;
            else{currentScreenV = 0;};
            break;
          case up:
            if(currentScreenV <= 0){currentScreenV = vMax -1;}else{
            currentScreenV--;
            }
            break;
          case right:
            nextScreen();
            break;
           
        }
        break;//end case 3
      case 4:
        switch(buttonPressed){
          case left:
            currentScreenH= 1; currentScreenV = 0; vMax=3;
            break;
          case down:
            if(currentScreenV+1 < vMax)
              currentScreenV++;
            else{currentScreenV = 0;};
            break;
          case up:
            if(currentScreenV <= 0){currentScreenV = vMax -1;}else{
            currentScreenV--;
            }
            break;
          case right:
            lcd.cursor();
            nextScreen();
            break;
        }
        break;//end case 4
      case 5:
        switch(buttonPressed){
          case enter:
            currentScreenH = 4; currentScreenV=0; vMax=3;
            saveToo();
            lcd.noCursor();
            break;
          case left:
            if(cursorPosition == 0){}else{
              cursorPosition --;
              updoot = true;
            }
            break;
          case right:
            if(cursorPosition == 14){}else{
              cursorPosition ++;
              updoot = true;
            }
            break;
          case up:
            incriment();
            break;
          case down:
            reduce();
            break;
        }
        break;
      case 10:
      case 11:
        switch(buttonPressed){
          case left:
          currentScreenH= 3; currentScreenV = 0; vMax=2;
            break;
            
        }
        break;
      case 7:
        switch(buttonPressed){
          case left:
          currentScreenH= 4; currentScreenV = 0; vMax=5;
            break;
          case down:
            if(currentScreenV+1 < vMax)
              currentScreenV++;
            else{currentScreenV = 0;};
            break;
          case up:
            if(currentScreenV <= 0){currentScreenV = vMax -1;}else{
            currentScreenV--;
            }
            break;
          case right:
            nextScreen();
            break;
            
        }
        break;
    case 20:
    case 21:
    case 22:
    case 23:
    case 24:
      switch(buttonPressed){
        case enter:
            currentScreenH = 4; currentScreenV=0; vMax=5;
            saveTemp();
            lcd.noCursor();
            break;
        case right:
          if(theCursor == 1)
            theCursor = 0;
          else  
            theCursor = 1;
          printIT=true;
          break;
          
        case up:
        if(theCursor == 0)
          lowTemp++;
        else
          highTemp++;
        printIT=true;
        break;
        case down:
        if(theCursor == 0)
          lowTemp--;
        else
          highTemp--;
        printIT=true;
        break;
          
      }
      break;
    }
    
  }

  void erase(){
    for (int i = 0 ; i < 900 ; i++) {
    EEPROM.write(i, 0);
  }
  }
  void loop()
 { 
   
  //Serial.print("u");
//------------------new stuff-----------------
 if(millis()- previousMillisTemp > tempCheck){
  setTemp();
  previousMillisTemp = millis();
 }
  if(millis() - previousMillisTH > thStatusInt){
  checkState();
  checkConection();
  previousMillisTH = millis();
 }


   int packetSize = Udp.parsePacket();
  if(packetSize)//This is where we process the packets
  {
    IPAddress remote = Udp.remoteIP();


    // read the packet into packetBufffer
    Udp.read(packetBuffer,UDP_TX_PACKET_MAX_SIZE);
    packetBuffer[packetSize] = 13;

    packetChecker(packetSize, 0);
    isUdpPacket = true;
    parseString(packetBuffer);
    
    isUdpPacket = false;
    int packetSiz = Udp.parsePacket();
  if(packetSiz){
    Udp.read(packetBuffer,UDP_TX_PACKET_MAX_SIZE);
    packetChecker(packetSize, 1);
    }
  
    
  }
 
//--------------------------------------------
 printScreen();
 
  
  int sensorValue = analogRead(A3);

 
    switch (buttonPressed(&sensorValue)){
      case 1:
        navMenue(enter);
        Serial.println("enter");

        break;
      case 2:
        //right

        navMenue(right);
        break;
      case 3:
        //up

        navMenue(up);
        break;
      case 4:
        //left

        navMenue(left);
        Serial.println("left");
        break;
      case 5:
        //down

        navMenue(down);
        break;
 
    }
  
  
   /* delay(500);
    lcd.clear();
     lcd.print(sensorValue);*/
 }

 int buttonPressed(int *value){
  
  
  int button = 0;
  if(millis() - previousMillisCoolDown > coolDownInterval){
    coolDownInterval = 0;
    if(*value >730 && *value <749){
      button = 1;//enter
      coolDownInterval = 500;
    }
    else if(*value > 490 && *value < 600){
      button = 2; //right
      coolDownInterval = 500;
    }
    else if(*value > 140 && *value <= 300){
      button = 3; //up
      coolDownInterval = 500;
    }
    else if(*value == 0 ){
      button = 4; //left
      coolDownInterval = 500;
    }
    else if(*value > 310 && *value < 400){
      button = 5; //down
      coolDownInterval = 500;
    }
    previousMillisCoolDown = millis();
  }
  return button;
 
  
 }

#define t_LED 2
#define t_D13 3
#define t_ON 4
#define t_OFF 5
#define t_GREEN 6
#define t_SET 9
#define t_STATUS 10
#define t_LEDS 11
#define t_VERSION 12
#define t_HELP 113
#define t_RED 14
#define t_BLINK 7
#include <stdio.h>
byte blinkBigLedIt = 0;
byte color = t_RED;
char lookup[12][5];
byte onBoardLed = 0;

void newLine(){
        Serial.write(13);
      Serial.write("\n");
}
void addToList(byte spot, byte wLength, char string[], byte last){
  byte i = 0;
  for(i = 0; i < 2; i++){
    lookup[spot][i]= string[i];
  }
  lookup[spot][i] = wLength;
  i++;

  lookup[spot][i] = last;

}

void printHelp(){
  Serial.write("List of Commands");
  newLine();
  Serial.write("--D13 ON");
  newLine();
  Serial.write("--D13 OFF");
  newLine();
  Serial.write("--D13 BLINK");
  newLine();
  Serial.write("--LED GREEN");
  newLine();
  Serial.write("--LED RED");
  newLine();
  Serial.write("--LED OFF");
  newLine();
  Serial.write("--LED BLINK");
  newLine();
  Serial.write("--STATUS LEDS");
  newLine();
  Serial.write("--VERSION");
  newLine();
  Serial.write("--HELP");
  newLine();
  Serial.write("");  
}


char inData[20]; // Allocate some space for the string
// constants won't change. Used here to set a pin number:
const byte ledPin =  LED_BUILTIN;// the number of the LED pin

int ledState = LOW;             // ledState used to set the LED
unsigned long previousMillis = 0;        // will store last time LED was updated
unsigned long previousMillisBig =0;      // will store last time LED was updated
long newInterval = 0;
long interval = 500;           // interval at which to blink (milliseconds)
char token[4];
char inChar; // Where to store the character read
byte index = 0; // Index into array; where to store the character
byte blinkIt = 0;
char command[3];
byte cmdIndex = 0;
const int pin_red=2;
const int pin_green=3;
int bigledState = LOW;
void searchComand(){
  switch(command[0]){
    case t_LED:
      switch(command[1]){
        case t_BLINK:
          blinkBigLedIt=1;
          break;
        case t_GREEN:
          //set led green
          color = t_GREEN;
          break;
        case t_RED:
          //set led red
          color = t_RED;
          break;
        case t_OFF:
          bgLedOff();
          blinkBigLedIt = 0;
          break;
      }
      break;
    case t_D13:
      switch(command[1]){
        case t_BLINK:
          blinkIt=1;
          break;
        case t_ON:
          //set led on
          ledOn();
          blinkIt = 0;
          break;
        case t_OFF:
          //set led off
          ledOff();
          blinkIt = 0;
          break;
      }
      break;
    case t_SET:
      switch(command[1]){
        case t_BLINK:
          interval = newInterval;
          break;
      }
      break;
    case t_STATUS:
      switch(command[1]){
        case t_LEDS:
          printStatus();
          break;
      }
      break;
    case t_VERSION:
      //print version
      Serial.write("Version 1.0");
      newLine();
      break;
    case t_HELP:
      //print help
      printHelp();
      break;
      
  }
  
}

void blinkLed(){
  unsigned long currentMillis = millis();

  if (currentMillis - previousMillis >= interval) {
    // save the last time you blinked the LED
    previousMillis = currentMillis;

    // if the LED is off turn it on and vice-versa:
    if (ledState == LOW) {
      ledState = HIGH;
    } else {
      ledState = LOW;
    }

    // set the LED with the ledState of the variable:
    digitalWrite(ledPin, ledState);
  }
  
}
void ledOff(){
  onBoardLed = 0;
  if(ledState == HIGH){
    ledState = LOW;
    digitalWrite(ledPin, ledState);
  }
}
void ledOn(){
  onBoardLed = 1;
    if(ledState == LOW){
    ledState = HIGH;
    digitalWrite(ledPin, ledState);
    }
}
void setRed(){
  //2 is high 
  //3 is low
}
void setGreen(){
  //3 is low
  //2 is high
  
}
void printStatus(){
  if(blinkBigLedIt == 1){
    Serial.write("LED is blinking ");
    if(color == t_RED){
      Serial.write("red");
    }else{
      Serial.write("green");
    }
    newLine();
  }else{
    Serial.write("LED is off");
    newLine();
  }
  if(blinkIt == 1){
    Serial.write("On board LED is blinking at ");
    Serial.print(interval);
    newLine();
  }
  else if(onBoardLed == 1){
    Serial.write("On board LED is ON");
    newLine();
    
  }else{
    Serial.write("On board LED is OFF");
    newLine();
  }
}
void blinkBigLed(){

   unsigned long currentMillis = millis();

  if (currentMillis - previousMillisBig >= interval) {
    // save the last time you blinked the LED
    previousMillisBig = currentMillis;

    // if the LED is off turn it on and vice-versa:
    if (bigledState == LOW) {
      bgLedOn();
      bigledState = HIGH;
    } else {
      bgLedOff();
      //ledState = LOW;
      bigledState = LOW;
    }

    // set the LED with the ledState of the variable:
    
  }
  
}
void bgLedOff(){
  digitalWrite(pin_green, LOW);
  digitalWrite(pin_red, LOW);
}
void bgLedOn(){
  if(color == t_RED){
  digitalWrite(pin_green, LOW);
  digitalWrite(pin_red, LOW);
  digitalWrite(pin_red, HIGH);
  }else{
      digitalWrite(pin_green, LOW);
  digitalWrite(pin_red, LOW);
  digitalWrite(pin_green, HIGH);
  }
}

void parseString(){
  cmdIndex = 0;
  for(byte k =0; k<3;k++){
    command[k] = 255;
  }
  int wordLength = 0;
  byte i = 0;
  byte j = 0;
  char toToken[10];
  
  while(inData[i] != 13){
   
    
    if(inData[i] == 32){
    //code in here
    toToken[j]= '\0';
      newLine();
        byte l=0;
  while(toToken[l] != 0){
 
    l++;
  }
  makeToken(wordLength, toToken);

      wordLength = 0;
      j = -1;
    }else{
      toToken[j] = inData[i];
      //Serial.write(inData[i]);
      wordLength++;
    }
    i++;
    j++;
  }
  toToken[j]= '\0';
  newLine();
  makeToken(wordLength, toToken);
 
  searchComand();

}
byte isNumber(byte wordLength, char toToken[]){
  byte isToken =0;
  for(int i = 0; i < wordLength; i++){
    if(toToken[i] >= 48 && toToken[i] <= 57){
      isToken = 1;
      
    }else{
      isToken = 0;
    }
  }
  
  return isToken;
}
void makeToken(byte wordLength, char toToken[]){
  if(isNumber(wordLength, toToken) == 1){
    sscanf(toToken, "%d", &newInterval);
  }
  for(byte i=0; i < 12; i++){

    if(toToken[0] == lookup[i][0]){

      if(toToken[1] == lookup[i][1]){
        if(wordLength == lookup[i][2]){
          command[cmdIndex] = lookup[i][3];
          cmdIndex++;
          break;
        }
      }

    }
  }

}

void setup() {
  pinMode(pin_red,OUTPUT);
pinMode(pin_green,OUTPUT);
  // set the digital pin as output:
  char led[] = "LED";
  char blnk[] = "BLINK";

  
   Serial.begin(9600);  
  pinMode(ledPin, OUTPUT);
  addToList(0, 3, led, t_LED);
  addToList(1, 5, blnk, t_BLINK);
  char on[] = "ON";
  addToList(2, 2, on, t_ON );
  char off[] = "OFF";
  addToList(3, 3, off, t_OFF);
  char green[] = "GREEN";
  addToList(4, 5, green, t_GREEN);
  char d13[] = "D13";
  addToList(5, 3, d13, t_D13);
  char red[] = "RED";
  addToList(6, 3, red, t_RED);
  char stat[] = "STATUS";
  addToList(7, 6, stat, t_STATUS);
  char leds[] = "LEDS";
  addToList(8, 4, leds, t_LEDS);
  char ver[] = "VERSION";
  addToList(9, 7, ver, t_VERSION);
  char help[] = "HELP";
  addToList(10, 4, help, t_HELP);
  char set[] = "SET";
  addToList(11, 3, set, t_SET);

  

  
}

void loop() {
  // here is where you'd put code that needs to be running all the time.

  // check to see if it's time to blink the LED; that is, if the difference
  // between the current time and last time you blinked the LED is bigger than
  // the interval at which you want to blink the LED.
  if(blinkIt == 1){
    blinkLed();
  }
 if(blinkBigLedIt == 1){
  blinkBigLed();
 }

    while(Serial.available() > 0) // Don't read unless
                                                 // there you know there is data
  {
      if(index < 19) // One less than the size of the array
      {
          inChar = Serial.read(); // Read a character
          Serial.write(inChar);
          inData[index] = inChar; // Store it
          index++; // Increment where to write next
          inData[index] = '\0'; // Null terminate the string
          if(inChar == 13){
            index = 0;
            parseString();
          }

      }
      if(index == 19){
            index = 0;
            newLine();
            Serial.write("command to long");
            newLine();
      }
      
  }
}
